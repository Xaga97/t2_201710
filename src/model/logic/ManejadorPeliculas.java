package model.logic;

import model.data_structures.ILista;
import model.data_structures.ListaDobleEncadenada;
import model.data_structures.ListaEncadenada;
import model.vo.VOAgnoPelicula;
import model.vo.VOPelicula;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import com.opencsv.CSVReader;

import api.IManejadorPeliculas;

public class ManejadorPeliculas implements IManejadorPeliculas {

	private ListaEncadenada<VOPelicula> misPeliculas;
	
	private ListaDobleEncadenada<VOAgnoPelicula> peliculasAgno;
	public void ordenarPeliculas(){
		VOAgnoPelicula[]m= new VOAgnoPelicula[peliculasAgno.darNumeroElementos()];
		for(int i =0;i<peliculasAgno.darNumeroElementos();i++){
			m[i]=peliculasAgno.darElemento(i);
		}
		VOAgnoPelicula auxiliar[] = new VOAgnoPelicula[peliculasAgno.darNumeroElementos()];
		for(int v = 2; v/2<peliculasAgno.darNumeroElementos();v*=2){
			for(int posi=0;posi<peliculasAgno.darNumeroElementos() && posi+v/2 < peliculasAgno.darNumeroElementos();posi+=v){
				int posm=posi+v/2;
				int posf=Math.min(posi+v,peliculasAgno.darNumeroElementos());
				for(int i = posi, d = posm, pi=posi;i<posm||d<posf;){
					if(i==posm){
						auxiliar[pi++]=m[d++];
					}
					else if(d==posf){
						auxiliar[pi++]= m[i++];
					}else{
						if(m[i].getAgno()<m[d].getAgno()){
							auxiliar[pi++]=m[i++];
						}
						else{
							auxiliar[pi++]=m[d++];
						}
					}
				}
				for(int s = posi;s<posf;s++){
					m[s]=auxiliar[s];
				}	    	
			}
		}
		peliculasAgno.clear();
		for(int f=0;f<m.length;f++){
			peliculasAgno.agregarElementoFinal(m[f]);
		}
	}

	
	@Override
	public void cargarArchivoPeliculas(String archivoPeliculas) 
	{
		try
		{
			CSVReader reader = new CSVReader(new FileReader("./data/movies.csv"));
			String [] nextLine = reader.readNext();
		    while ((nextLine = reader.readNext()) != null) 
		    {
		    	String titulo ="";
		    	Integer agno = null;
		    	if( nextLine[1].indexOf("(") <= 0 )
		    	{
		    		titulo = nextLine[1];
		    	}
		    	else
		    	{
		    		String [] info = nextLine[1].split("(");
				    titulo = info[0];
				    agno = Integer.parseInt(info[1].replace(")", ""));
		    	}
		    	
		       String [] genresV = nextLine[2].split("|");
		       ListaEncadenada<String> genres = new ListaEncadenada<>();
		       for( int i = 0; i < genresV.length; i++)
		       {
		    	   genres.agregarElementoFinal(genresV[i]);
		       }
		       
		       VOPelicula peliculaNueva = new VOPelicula(titulo, agno, genres);
		       
		       if( agno != null)
		       {
		    	   VOAgnoPelicula agnoPelicula = new VOAgnoPelicula(agno);
		    	   VOAgnoPelicula elemBuscado = peliculasAgno.buscarElemento(agnoPelicula);
			       if( elemBuscado != null )
			       {
			    	   if( elemBuscado.getPeliculas() != null )
			    	   {
			    		   elemBuscado.getPeliculas().agregarElementoFinal(peliculaNueva);
			    	   }
			    	   else
			    	   {
			    		   ListaEncadenada<VOPelicula> peliculas = new ListaEncadenada<>();
			    		   peliculas.agregarElementoFinal(peliculaNueva);
			    		   elemBuscado.setPeliculas(peliculas);
			    	   }
			       }
			       else
			       {
			    	   ListaEncadenada<VOPelicula> peliculas = new ListaEncadenada<>();
		    		   peliculas.agregarElementoFinal(peliculaNueva);
			    	   agnoPelicula.setPeliculas(peliculas);
			    	   peliculasAgno.agregarElementoFinal(agnoPelicula);
			       }
		       }
		    }
		    ordenarPeliculas();
		}
		catch( FileNotFoundException e)
		{
			System.out.println("No se encontró el archivo: " + e.getStackTrace());
		} 
		catch (IOException e) 
		{
			System.out.println("Error en la lectura del archivo" + e.getStackTrace());
		}
		
	}

	@Override
	public ILista<VOPelicula> darListaPeliculas(String busqueda) {
		ListaEncadenada<VOPelicula> m = new ListaEncadenada<VOPelicula>();
		for(VOPelicula x:misPeliculas){
			if(x.getTitulo().contains(busqueda))m.agregarElementoFinal(x);
		}
		return m;
	}

	@Override
	public ILista<VOPelicula> darPeliculasAgno(int agno) {
		ListaEncadenada<VOPelicula> m = new ListaEncadenada<VOPelicula>();
		for(VOPelicula x:misPeliculas){
			if(x.getAgnoPublicacion()==agno)m.agregarElementoFinal(x);
		}
		return m;
	}

	@SuppressWarnings("null")
	@Override
	public VOAgnoPelicula darPeliculasAgnoSiguiente() {
		VOAgnoPelicula m = null;
		ListaEncadenada<VOPelicula> s = new ListaEncadenada<VOPelicula>();
		
		m.setAgno(misPeliculas.darElementoPosicionActual().getAgnoPublicacion()+1);
		for(VOPelicula x:misPeliculas){
			if(x.getAgnoPublicacion()==misPeliculas.darElementoPosicionActual().getAgnoPublicacion()+1){
				s.agregarElementoFinal(x);
			}
		}
		m.setPeliculas(s);
		return m;
	}

	@SuppressWarnings("null")
	@Override
	public VOAgnoPelicula darPeliculasAgnoAnterior() {
		VOAgnoPelicula m = null;
		ListaEncadenada<VOPelicula> s = new ListaEncadenada<VOPelicula>();
		
		m.setAgno(misPeliculas.darElementoPosicionActual().getAgnoPublicacion()-1);
		for(VOPelicula x:misPeliculas){
			if(x.getAgnoPublicacion()==misPeliculas.darElementoPosicionActual().getAgnoPublicacion()-1){
				s.agregarElementoFinal(x);
			}
		}
		m.setPeliculas(s);
		return m;
		
	}

}
