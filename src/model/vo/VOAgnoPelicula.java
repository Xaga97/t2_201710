package model.vo;

import model.data_structures.ILista;

public class VOAgnoPelicula implements Comparable<VOAgnoPelicula> {

	private int agno;
	private ILista<VOPelicula> peliculas;
	
	public VOAgnoPelicula( int pAgno )
	{
		agno = pAgno;
	}
	
	public int getAgno() {
		return agno;
	}
	public void setAgno(int agno) {
		this.agno = agno;
	}
	public ILista<VOPelicula> getPeliculas() {
		return peliculas;
	}
	public void setPeliculas(ILista<VOPelicula> peliculas) {
		this.peliculas = peliculas;
	}

	public int compareTo(VOAgnoPelicula pAgnoPelicula) 
	{
		if( pAgnoPelicula.getAgno() == agno )
		{
			return 1;
		}
		else
		{
			return 0;
		}
	}
	
	
}
